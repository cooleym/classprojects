Shader "CustomShaders/Function" {
	Properties {
		_ColorMain ("Main Color", Color) = (1,1,1,0.5)
		_ColorLine ("Line Color", Color) = (1,1,1,0.5)
		_LineWidth ("Line Width", Float) = 0.0
		_ColorWater ("Line Color", Color) = (1,1,1,0.5)
		_WaterHeight("Water Height", Float) = 0.0
		_WaterRange("WaterRange", Float) = 0.0
	}
	SubShader {
		Pass { // some shaders require multiple passes
			 CGPROGRAM // here begins the part in Unity's Cg
			 #include "UnityCG.cginc"
	 
			 #pragma vertex vert 
				// this specifies the vert function as the vertex shader 
			 #pragma fragment frag
				// this specifies the frag function as the fragment shader
				
			 struct vertexOutput {
				float4 pos : SV_POSITION;
				float4 col : TEXCOORD0;
				float4 position_in_world_space : TEXCOORD1;
			 };
			 
			 float _Slope;
			 float _InitHeight;
			 float _LineWidth;
			 float _WaterHeight;
			 float _WaterRange;
			 float4 _ColorMain;
			 float4 _ColorLine;
			 float4 _ColorWater;
	 
			 vertexOutput vert(float4 vertexPos : POSITION)
				// vertex shader 
			 {
				vertexOutput output; // we don't need to type 'struct' here
 
				output.pos =  mul(UNITY_MATRIX_MVP, vertexPos);
				output.col = _ColorMain;
				output.position_in_world_space =  mul(_Object2World, vertexPos);
				   // Here the vertex shader writes output data
				   // to the output structure. We add 0.5 to the 
				   // x, y, and z coordinates, because the 
				   // coordinates of the cube are between -0.5 and
				   // 0.5 but we need them between 0.0 and 1.0. 
				return output;
			 }
	 
			 float4 frag(vertexOutput input) : COLOR // fragment shader
			 {
				 float4 outColor = input.col;
				 
				 float eqn;
				 float x = input.position_in_world_space.x;
				 float waveHeight = sin(25*_Time + x/10);
				 eqn =90 * (pow(2, -1 * pow((x + 40) / (27.0719976425171 ), 2)) + pow(2, -1 * pow((x - 40) / (27.0719976425171 ), 2))) + 3.5 * (0.7 * (round(sin(x) + sin(.5 * x))) + (1 - 0.7) * (sin(x) + sin(.5 * x))) - 30;
				 
				 if(input.position_in_world_space.y > (eqn - _LineWidth))
				 {
					 outColor = _ColorLine;
				 }
				 if(input.position_in_world_space.y > (eqn + _LineWidth))
				 {
					 if(input.position_in_world_space.y < _WaterHeight + waveHeight
					 &&(_WaterRange == 0 || abs(x)<_WaterRange))
					 {
						 outColor = _ColorWater;
					 }
					 else
					 {
						discard; 
					 }
				 }
				 
				return outColor;
               // Here the fragment shader returns the "col" input 
               // parameter with semantic TEXCOORD0 as nameless
               // output parameter with semantic COLOR.
			 }
	 
			 ENDCG // here ends the part in Cg 
		} 
	}
	FallBack "Diffuse"
}

