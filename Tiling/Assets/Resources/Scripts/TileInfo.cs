﻿using UnityEngine;
using System.Collections;
using OhioState.Tiling;

public class TileInfo : MonoBehaviour {

    public float WidthX;
    public float HeightY;
    public float DepthZ;

    private GameObjTile Tile;

    void Awake()
    {
        this.Tile = new GameObjTile(this.gameObject);
    }

    void Update()
    {
        WidthX = Tile.WidthX;
        HeightY = Tile.HeightZ;
    }
}
