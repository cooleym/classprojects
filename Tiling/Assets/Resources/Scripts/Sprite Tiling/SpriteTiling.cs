﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace OhioState.Tiling
{
    class SpriteTiling : ITiling<Sprite>
    {
        
        public int Width
        {
            get { return width; }
        }
        public int Height
        {
            get { return height; }
        }
        public int NumberOfTiles
        {
            get { return width* height; }
        }
        
        public SpriteTiling(List<SpriteTile> tiling, int width, int height)
        {
            Layout = tiling;
            this.width = width;
            this.height = height;
        }
        public void SetTile(int i, int j, SpriteTile tile)
        {
            if (i >= 0 && i < width && j >= 0 && j < height)
            {
                Layout[j * width + i] = tile;
            }
            else
                throw new Exception("tiling index out of range");
        }
        public SpriteTile GetTile(int i, int j)
        {
            if (i >= 0 && i < width && j >= 0 && j < height)
            {
                return Layout[j * width + i];
            }
            else
                throw new Exception("tiling index out of range");
        }
        private int width;
        private int height; 
        private List<SpriteTile> Layout;


        
    }
}
