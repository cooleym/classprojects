﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OhioState.Tiling;
using UnityEngine;

namespace OhioState.Tiling
{
    class SpriteTile:ITile<Sprite>
    {
        public int EdgeCount
        {
            get { return 4; }
        }
        public float Grass
        {
            get { return grass; }
        }
        public float Earth
        {
            get { return earth; }
        }
        public Sprite TileData
        {
            get { return tileData; }
        }
        public SpriteTile(Sprite tileData, float grass, float earth)
        {
            this.tileData = tileData;
            this.grass = grass;
            this.earth = earth;
        }
    
        
        private Sprite tileData;
        private float grass;
        private float earth;
    }
}
