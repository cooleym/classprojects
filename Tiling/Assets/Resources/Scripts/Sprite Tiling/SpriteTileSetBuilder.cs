﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using OhioState.Tiling;

namespace OhioState.Tiling
{
    class SpriteTileSetBuilder:ITileSetBuilder<Sprite>
    {
        public SpriteTileSetBuilder(int width, int height)
        {
            this.width = width;
            this.height = height;
        }
        public ITileSet<Sprite> Build()
        {
            //Create textures for sprite
            Texture2D Ground = new Texture2D(width, height, TextureFormat.RGBA32, true);
            Texture2D GroundGrass = new Texture2D(width, height, TextureFormat.RGBA32, true);
            Texture2D GroundGrassSky_1 = new Texture2D(width, height, TextureFormat.RGBA32, true);
            Texture2D GroundGrassSky_2 = new Texture2D(width, height, TextureFormat.RGBA32, true);
            Texture2D GroundGrassSky_3 = new Texture2D(width, height, TextureFormat.RGBA32, true);

            Color grass = new Color(1.0f / 255, 166.0f / 255, 17.0f / 255.0f, 1.0f);
            Color ground = new Color(94.0f / 255.0f, 65.0f / 255.0f, 47.0f / 255.0f, 1.0f);
            Color transparent = new Color(0.0f, 0.0f, 0.0f, 0.0f);

            //Set the uncolored pixel transparent to avoid artifact
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    if (j > height * 0.75f)
                    {
                        Ground.SetPixel(i, j, ground);
                        GroundGrass.SetPixel(i, j, grass);
                        GroundGrassSky_1.SetPixel(i, j, transparent);
                        GroundGrassSky_2.SetPixel(i, j, transparent);
                        GroundGrassSky_3.SetPixel(i, j, transparent);
                    }
                    else if (j > height / 2)
                    {
                        Ground.SetPixel(i, j, ground);
                        GroundGrass.SetPixel(i, j, ground);
                        GroundGrassSky_1.SetPixel(i, j, grass);
                        GroundGrassSky_2.SetPixel(i, j, transparent);
                        GroundGrassSky_3.SetPixel(i, j, transparent);
                    }
                    else if (j > height / 4)
                    {
                        Ground.SetPixel(i, j, ground);
                        GroundGrass.SetPixel(i, j, ground);
                        GroundGrassSky_1.SetPixel(i, j, ground);
                        GroundGrassSky_2.SetPixel(i, j, grass);
                        GroundGrassSky_3.SetPixel(i, j, transparent);
                    }
                    else
                    {
                        Ground.SetPixel(i, j, ground);
                        GroundGrass.SetPixel(i, j, ground);
                        GroundGrassSky_1.SetPixel(i, j, ground);
                        GroundGrassSky_2.SetPixel(i, j, ground);
                        GroundGrassSky_3.SetPixel(i, j, grass);
                    }
                }
            }

            //No warping
            Ground.wrapMode = TextureWrapMode.Clamp;
            GroundGrass.wrapMode = TextureWrapMode.Clamp;
            GroundGrassSky_1.wrapMode = TextureWrapMode.Clamp;
            GroundGrassSky_2.wrapMode = TextureWrapMode.Clamp;
            GroundGrassSky_3.wrapMode = TextureWrapMode.Clamp;

            Ground.Apply();
            GroundGrass.Apply();
            GroundGrassSky_1.Apply();
            GroundGrassSky_2.Apply();
            GroundGrassSky_3.Apply();

            tiles = new List<SpriteTile>();

            Sprite groundSprite = Sprite.Create(Ground, new Rect(0.0f, 0.0f, width, height), new Vector2(0.5f, 0.5f));
            tiles.Add(new SpriteTile(groundSprite, 0.0f, 1.0f));

            Sprite groundGrassSprite = Sprite.Create(GroundGrass, new Rect(0.0f, 0.0f, width, height), new Vector2(0.5f, 0.5f));
            tiles.Add(new SpriteTile(groundGrassSprite, 0.25f, 0.75f));

            Sprite groundGrassSky_1Sprite = Sprite.Create(GroundGrassSky_1, new Rect(0.0f, 0.0f, width, height), new Vector2(0.5f, 0.5f));
            tiles.Add(new SpriteTile(groundGrassSky_1Sprite, 0.25f, 0.5f));

            Sprite groundGrassSky_2Sprite = Sprite.Create(GroundGrassSky_2, new Rect(0.0f, 0.0f, width, height), new Vector2(0.5f, 0.5f));
            tiles.Add(new SpriteTile(groundGrassSky_2Sprite, 0.25f, 0.25f));

            Sprite groundGrassSky_3Sprite = Sprite.Create(GroundGrassSky_3, new Rect(0.0f, 0.0f, width, height), new Vector2(0.5f, 0.5f));
            tiles.Add(new SpriteTile(groundGrassSky_3Sprite, 0.25f, 0.0f));

            SpriteTileSet tileSet = new SpriteTileSet(tiles, width, height);

            return (ITileSet<Sprite>)tileSet;
        }
        private List<SpriteTile> tiles;
        private int width;
        private int height;
    }
}
