﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OhioState.Tiling;
using UnityEngine;

namespace OhioState.Tiling
{
    class SpriteTileSet:ITileSet<Sprite>
    {
        public int Width
        {
            get { return width; }
        }
        public int Height
        {
            get { return height; }
        }
        public ITile<Sprite> GetTile(int index)
        {
            if (index < 0 || index >= numberOfTiles)
                throw new Exception("Tile index is invalid");
            return tiles[index];
        }

        public int NumberOfTiles
        {
            get { return numberOfTiles; }
        }
        
        public SpriteTile GetTile(float grass, float earth)
        {
            
            bool exist  = tiles.Exists(x => (x.Grass == grass && x.Earth == earth));
            if (exist == false)
            {
                throw new Exception("tile doesn't exists with grass: " + grass.ToString() + " earth: " + earth.ToString());
            }
            SpriteTile tile = tiles.Find(x => (x.Grass == grass && x.Earth == earth));
            return tile;
        }
        
        public SpriteTileSet(List<SpriteTile> tiles, int width, int height)
        {
            this.tiles = tiles;
            this.numberOfTiles = tiles.Count;
            this.width = width;
            this.height = height;
        }
        private List<SpriteTile> tiles;
        private int numberOfTiles;
        private int width;
        private int height;
    }
}
