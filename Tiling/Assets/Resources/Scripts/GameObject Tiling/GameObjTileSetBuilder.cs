﻿using UnityEngine;
using System.Collections;
using OhioState.Tiling;

public class GameObjTileSetBuilder : ITileSetBuilder<GameObject>
{
    public GameObjTileSet TileSet;
    public TileIndex2D MapDimensions;
    public Texture2D RawTileMap;
    public bool Wang;
    private int TilePixelWidth, TilePixelHeight;
    private int EdgesPerTile;

    public GameObjTileSetBuilder(Texture2D mapImage, TileIndex2D mapDimensions, bool wang)
    {
        MapDimensions = mapDimensions;
        RawTileMap = mapImage;
        Wang = wang;
        TilePixelWidth = RawTileMap.width / mapDimensions.X;
        TilePixelHeight = RawTileMap.height / mapDimensions.Z;
    }

    public ITileSet<GameObject> Build()
    {
        //if(Wang) MapDimensions = new TileIndex2D(4,4);
        //else MapDimensions = new TileIndex2D(4,4);

        TileSet = new GameObjTileSet(RawTileMap, MapDimensions);

        SetUpTiles();
        SetUpEdges();
        return TileSet;
    }

    private void SetUpEdges()
    {
        //For each edge in each tile
        for (int i = 0; i < MapDimensions.Z; i++)
        {
            for (int j = 0; j < MapDimensions.X; j++)
            {
                for (int k = 0; k < TileSet.Edges.GetLength(2); k++)
                {
                    TileSet.Edges[j, i, k] = DetermineEdgeType(new TileIndex2D(j, i), GameObjTile.GetNumericEdge(k));
                }
            }
        }
    }

    private void SetUpTiles()
    {
        for (int i = 0; i < MapDimensions.Z; i++)
        {
            for (int j = 0; j < MapDimensions.X; j++)
            {
                Texture2D tile = new Texture2D(TilePixelWidth, TilePixelHeight);
                int offsetX, OffsetY;

                offsetX = j * TilePixelWidth;
                OffsetY = i * TilePixelHeight;
                Color[] tex = TileSet.RawTileMap.GetPixels(offsetX, OffsetY, TilePixelWidth, TilePixelHeight);
                tile.SetPixels(tex);
                tile.Apply();
                TileSet.Tiles[j, i] = tile;
            }
        }
    }

    private bool DetermineEdgeType(TileIndex2D index, GameObjTile.Edge edge)
    {
        int offsetX, offsetY;
        if (edge == GameObjTile.Edge.North || edge == GameObjTile.Edge.South)
        {
            offsetX = TilePixelWidth / 2;
            //Assuming coordinate system has origin in bottom left corner
            if (edge == GameObjTile.Edge.North) offsetY = TilePixelHeight - 1;
            else offsetY = 0;
        }
        else if (edge == GameObjTile.Edge.East || edge == GameObjTile.Edge.West)
        {
            offsetY = TilePixelHeight / 2;
            if (edge == GameObjTile.Edge.East) offsetX = TilePixelWidth - 1;
            else offsetX = 0;
        }
        else return false;

        Color pixelColor = TileSet.Tiles[index.X, index.Z].GetPixel(offsetX, offsetY);

        //return whether or not the edge is connected or disconnected
        return pixelColor != Color.black;
    }


}
