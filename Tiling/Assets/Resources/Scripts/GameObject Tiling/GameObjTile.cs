﻿using UnityEngine;
using System.Collections;
using OhioState.Tiling;

public class GameObjTile : ITile<GameObject>
{
    public enum Edge
    {
        North,
        South,
        East,
        West,
        Top,
        Bottom
    }

    public int EdgeCount
    {
        get;
        set;
    }

    public GameObject TileData
    {
        get { return tileData; }
    }

    public GameObjTile(GameObject tileData)
    {
        this.tileData = tileData;
    }

    private GameObject tileData;
    public TileIndex2D tileSetIndex;
    public float HeightZ { get { return tileData.renderer.bounds.extents.z * 2f; } }
    public float WidthX { get { return tileData.renderer.bounds.extents.x * 2f; } }
    public float DepthY { get { return tileData.renderer.bounds.extents.y * 2f; } }


    public static Edge GetNumericEdge(int i)
    {
        Edge edge = Edge.North;
        if (i == 1) edge = Edge.East;
        if (i == 2) edge = Edge.Top;
        if (i == 3) edge = Edge.South;
        if (i == 4) edge = Edge.West;
        if (i == 5) edge = Edge.Bottom;

        return edge;
    }

    public static int GetNumericEdge(Edge edge)
    {
        int i = 0;
        if (edge == Edge.East) i = 1;
        if (edge == Edge.Top) i = 2;
        if (edge == Edge.South) i = 3;
        if (edge == Edge.West) i = 4;
        if (edge == Edge.Bottom) i = 5;

        return i;
    }

    public static Edge GetAdjacentEdge(Edge edge)
    {
        int i = GetNumericEdge(edge);
        i = (i + 3) % 6;
        return GetNumericEdge(i);
    }
}