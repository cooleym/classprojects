﻿using UnityEngine;
using System.Collections;
using OhioState.Tiling;

public class GameObjTiling : ITiling2D<GameObject>
{
    public int NumTilesX { get; private set; }
    public int NumTilesY { get; private set; }
    public int NumTilesZ { get; private set; }

    public int NumberOfTiles { 
        get 
        { 
            if(NumTilesY == 0) return NumTilesX * NumTilesZ;
            if (NumTilesZ == 0) return NumTilesX;
            return NumTilesX * NumTilesZ * NumTilesY;
        } 
    }

    public GameObjTile[] Tiling1d;
    public GameObjTile[,] Tiling2d;
    public GameObjTile[,,] Tiling3d;

    public GameObjTiling(int dimX)
    {
        NumTilesX = dimX;
        Tiling1d = new GameObjTile[dimX];
    }

    public GameObjTiling(TileIndex2D dimensions)
    {
        NumTilesX = dimensions.X;
        NumTilesZ = dimensions.Z;

        Tiling2d = new GameObjTile[NumTilesX, NumTilesZ];
    }

    public GameObjTiling(int dimX, int dimZ)
    {
        NumTilesX = dimX;
        NumTilesZ = dimZ;
        Tiling2d = new GameObjTile[dimX, dimZ];
    }

    public GameObjTiling(int dimX, int dimY, int dimZ)
    {
        NumTilesX = dimX;
        NumTilesY = dimY;
        NumTilesZ = dimZ;
        Tiling3d = new GameObjTile[dimX, dimY, dimZ];
    }

    public ITile<GameObject> GetTile(TileIndex2D index)
    {
        return Tiling2d[index.X, index.Z];
    }

    public ITile<GameObject> GetTile(int x, int z)
    {
        return Tiling2d[x, z];
    }

    public ITile<GameObject> GetTile(int x)
    {
        return Tiling1d[x];
    }

    public ITile<GameObject> GetTile(int x, int y, int z)
    {
        return Tiling3d[x, y, z];
    }

    public void SetTile(TileIndex2D index, GameObjTile tile)
    {
        Tiling2d[index.X, index.Z] = tile;
    }

    public void SetTile(int x, int z, GameObjTile tile)
    {
        Tiling2d[x, z] = tile;
    }

    public void SetTile(int x, GameObjTile tile)
    {
        Tiling1d[x] = tile;
    }

    public void SetTile(int x, int y, int z, GameObjTile tile)
    {
        Tiling3d[x, y, z] = tile;
    }
}