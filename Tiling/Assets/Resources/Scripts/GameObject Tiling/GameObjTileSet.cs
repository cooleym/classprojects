﻿using UnityEngine;
using System.Collections;
using OhioState.Tiling;

public class GameObjTileSet : ITileSet<GameObject>
{
    public int NumberOfTiles { get; private set; }
    public Texture2D[,] Tiles;
    public bool[, ,] Edges;
    public Texture2D RawTileMap;

    public GameObjTileSet(Texture2D map, TileIndex2D dim)
    {
        NumberOfTiles = dim.X * dim.Z;
        RawTileMap = map;
        Tiles = new Texture2D[dim.X, dim.Z];
        Edges = new bool[dim.X, dim.Z, 6];
    }

    public ITile<GameObject> GetTile(int index)
    {
        throw new System.NotImplementedException();
    }

    public GameObject CreateGameObjectTile(TileIndex2D index)
    {
        GameObject tile = GameObject.CreatePrimitive(PrimitiveType.Plane);
        Material m = new Material(tile.renderer.material);
        m.SetTexture(0, Tiles[index.X, index.Z]);
        tile.renderer.material = m;
        return tile;
    }
}