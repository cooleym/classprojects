﻿using UnityEngine;
using System.Collections;

public class FollowMouse : MonoBehaviour {

    [Range(0,10)]public float LookSensitivity;
    [Range(0,10)]public float ZoomSensitivity;
    public bool InvertedControls;
    private Vector3 PrevMousePosition;
    private float OrigCameraSize;
    private float ZoomFactor;

	// Use this for initialization
	void Start () {
        PrevMousePosition = Input.mousePosition;
        OrigCameraSize = Camera.main.orthographicSize;
        ZoomFactor = 1;
	}
	
	// Update is called once per frame
	void Update () {
        ZoomFactor = Camera.main.orthographicSize / OrigCameraSize;
        if(!Input.GetKey(KeyCode.Space)) MoveCameraWithMouse();
        ScrollCamera();
        PrevMousePosition = Input.mousePosition;
	}

    void MoveCameraWithMouse()
    {
        Vector3 MoveDir;
        if(InvertedControls)  MoveDir = new Vector3(Input.mousePosition.x - PrevMousePosition.x, 0, Input.mousePosition.y - PrevMousePosition.y);
        else MoveDir = new Vector3(PrevMousePosition.x - Input.mousePosition.x, 0, PrevMousePosition.y - Input.mousePosition.y);
        this.transform.position -= MoveDir * LookSensitivity * Time.deltaTime * ZoomFactor;
    }

    void ScrollCamera()
    {
        Camera.main.orthographicSize -= Input.mouseScrollDelta.y * ZoomSensitivity * ZoomFactor;
    }
}
