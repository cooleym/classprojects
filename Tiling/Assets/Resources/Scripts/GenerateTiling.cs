﻿using UnityEngine;
using System.Collections;
using OhioState.Tiling;

public class GenerateTiling : MonoBehaviour
{

    private GameObjTilingBuilder TilingBuilder;
    private GameObjTileSetBuilder SetBuilder;
    private GameObjTileSet TileSet;
    public GameObject Tile;
    public Texture2D TileSetMap;
    public Vector2 Dimensions;
    public bool RandDir;
    public bool Build;

    private GameObject[,] TilesArray;

    // Use this for initialization
    void Awake()
    {
        InitBuilder();
    }

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        if (Build)
        {
            BuildTilingData();
            Build = false;
        }
    }

    private void InitBuilder()
    {
        if (TileSetMap != null)
        {
            SetBuilder = new GameObjTileSetBuilder(TileSetMap, new TileIndex2D(4, 4), true);
            TileSet = (GameObjTileSet)SetBuilder.Build();
        }

        TilingBuilder = new GameObjTilingBuilder(Tile, (int)Dimensions.x, (int)Dimensions.y, 0);
    }

    private void BuildTilingData()
    {
        if (TileSetMap == null) TilingBuilder.Build(RandDir);
        else TilingBuilder.BuildTiling(TileSet);
    }
}
