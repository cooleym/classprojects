using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OhioState.Tiling;
using UnityEngine;

namespace OhioState.Tiling
{
    class PerlinNoiseTile : Wang2DGridTile<Texture2D,List<int>>
    {
        public PerlinNoiseTile(List<int> topEdge, List<int> bottomEdge, List<int> leftEdge, List<int>rightEdge, Texture2D tileData)
            : base(topEdge, bottomEdge, leftEdge, rightEdge, tileData)
        {
 
        }
        //public override List<int> GetEdgeColor(Direction dir)
        //{
        //    return edgeColors[dir];
        //}
        public void Print()
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < tileData.width; i++)
            {
                
                for (int j = 0; j < tileData.height; j++)
                {
                    float c = tileData.GetPixel(i, j).r;
                    int noise = (int)(c*256.0f);
                    s.Append(noise.ToString() + "\t");
                }
                s.Append("\n");
            }
            Debug.Log(s.ToString());
        }
    }
}
