﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OhioState.Tiling;
using UnityEngine;

namespace OhioState.Tiling
{
    
    class TileRender : MonoBehaviour
    {
        public PerlinNoiseTile tile;
        public void Start()
        {
            

            Texture2D permTexture = tile.TileData;
            permTexture.filterMode = FilterMode.Point;
            permTexture.wrapMode = TextureWrapMode.Repeat;
            permTexture.Apply();

            Material perlinMaterial = transform.GetComponent<Renderer>().material;
            perlinMaterial.SetTexture("permSampler", permTexture);
           
            perlinMaterial.SetInt("noiseResolution", noiseResolution);

        }
        public void Update()
        {
            
        }
        private PerlinNoiseTileSet tileSet;
        private Texture2D permTexture;
        private Texture2D gradTexture;
        private int noiseResolution;
        
        
    }
}
