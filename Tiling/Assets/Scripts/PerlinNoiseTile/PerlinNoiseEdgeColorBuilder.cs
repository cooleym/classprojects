﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OhioState.Tiling;

namespace OhioState.Tiling
{
    public class PerlinNoiseEdgeColorBuilder : EdgeColorsBuilder<List<int>>
    {
        public PerlinNoiseEdgeColorBuilder( int resolution)
        {
            this.resolution = resolution;
            rng = new Random();
        }
        public List<List<int>> BuildEdgeColor(int numberOfColors)
        {
            List<List<int>> edgeColors = new List<List<int>>();
            
            for (int i = 0; i < numberOfColors; i++)
            {
                List<int> edgeColor = new List<int>();
                for (int j = 0; j < resolution; j++)
                {
                    int idx = rng.Next(256);
                    edgeColor.Add(idx);
                }
                
                edgeColor[0] = 125;
                edgeColor[resolution - 1] = 125;
                edgeColors.Add(edgeColor);
            }
            
            return edgeColors;
        }
        private int resolution;
        private Random rng;
    }
}
