﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OhioState.Tiling;
using UnityEngine;

namespace OhioState.Tiling
{
    class PerlinNoiseTileSet:Wang2DGridTileSet<Texture2D,List<int>>
    {
        public PerlinNoiseTileSet(List<ITile<Texture2D>> generatedTiles, List<List<int>> verticalColors, List<List<int>> horizontalColors)
            : base(generatedTiles, verticalColors, horizontalColors)
        {
            
        }

        public bool MatchEdges(List<int> c1, List<int> c2)
        {
            if (c1 != c2)
                return false;
            return true;
            //for (int i = 0; i < c1.Count(); i++)
            //    if (c1[i] != c2[i])
            //        return false;
            //return true;
        }
        public IEnumerable<PerlinNoiseTile> MatchingTiles(IEnumerable<Tuple<Direction, List<int>>> edgeColorConstraints)
        {
            foreach (PerlinNoiseTile tile in tiles)
            {
                PerlinNoiseTile wangTile = (PerlinNoiseTile)tile;
                bool allSatisfied = true;
                foreach (Tuple<Direction, List<int>> constraint in edgeColorConstraints)
                {
                    Direction dir = constraint.Item1;
                    List<int> result = wangTile.GetEdgeColor(dir);

                    if (MatchEdges(constraint.Item2, result) == false)
                    {
                        allSatisfied = false;
                        break;
                    }
                }
                if (allSatisfied == true)
                    yield return tile;
            }
        }
    }
}
