﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OhioState.Tiling;
using UnityEngine;

namespace OhioState.Tiling
{
    public class PerlinTilingScene : MonoBehaviour
    {
        public int noiseResolution;
        public int width;
        public int height;
        public int horizontalColors;
        public int verticalColors;
        public float tileWidth;
        public float tileHeight;
        void Start()
        {
            PerlinNoiseEdgeColorBuilder edgeColorBuilder = new PerlinNoiseEdgeColorBuilder(noiseResolution);
            PerlinNoiseTileSetBuilder builder = new PerlinNoiseTileSetBuilder(horizontalColors, verticalColors, noiseResolution, edgeColorBuilder);
            tileSet = (PerlinNoiseTileSet)builder.Build();

            PerlinNoiseTilingBuilder tilingBuilder = new PerlinNoiseTilingBuilder(width, height, tileSet);
            GridTiling<Texture2D> tiling = (GridTiling<Texture2D>)tilingBuilder.BuildTiling();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    PerlinNoiseTile tile = (PerlinNoiseTile)tiling.GetTile(y * width + x);
                    Texture2D permTexture = tile.TileData;
                    RenderTile(x, y, permTexture);
                    tile.Print();
                }
            }
           
            Camera.main.transform.position = new Vector3(width  * tileWidth, height / 2 * tileHeight, -50.0f);
            Camera.main.transform.rotation = Quaternion.identity;
        }
        void Update()
        {
 
        }

        void RenderTile(int x, int y, Texture2D permTexture)
        {
            GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
            
            plane.transform.Translate(x * tileWidth, y * tileHeight,0.0f);
            plane.transform.Rotate(270.0f, 0.0f, 0.0f);
            permTexture.Apply();
            
            permTexture.filterMode = FilterMode.Point;
            permTexture.wrapMode = TextureWrapMode.Clamp;
            Shader tileShader = Resources.Load("Materials/PerlinNoiseTile") as Shader;
            Material tileMaterial = new Material(tileShader);
            plane.transform.GetComponent<Renderer>().material = tileMaterial;
            plane.transform.GetComponent<Renderer>().material.SetTexture("permSampler", permTexture);
            plane.transform.GetComponent<Renderer>().material.SetInt("noiseResolution", noiseResolution);

            GameObject debugPlane = GameObject.CreatePrimitive(PrimitiveType.Plane);

            debugPlane.transform.Translate(x * tileWidth + width*tileWidth, y * tileHeight, 0.0f);
            debugPlane.transform.Rotate(270.0f, 0.0f, 0.0f);
            permTexture.Apply();

            permTexture.filterMode = FilterMode.Point;
            permTexture.wrapMode = TextureWrapMode.Clamp;
            Shader DebugShader = Resources.Load("Materials/DebugShader") as Shader;
            Material DebugMaterial = new Material(DebugShader);
            debugPlane.transform.GetComponent<Renderer>().material = DebugMaterial;
            debugPlane.transform.GetComponent<Renderer>().material.SetTexture("permSampler", permTexture);
            debugPlane.transform.GetComponent<Renderer>().material.SetInt("noiseResolution", noiseResolution);

        }  
        private PerlinNoiseTileSet tileSet;
    }
}
