﻿using UnityEngine;
using System.Collections;

public class Car : MonoBehaviour {

    public float MaxSpeed;
    public float CurrentSpeed;
    public float TurnSpeed;
    public float Accel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.A)) transform.Rotate(0, -TurnSpeed * Time.deltaTime, 0);
        if (Input.GetKey(KeyCode.D)) transform.Rotate(0, TurnSpeed * Time.deltaTime, 0);
        if (Input.GetKey(KeyCode.W)) CurrentSpeed += Accel * Time.deltaTime;
        else if (Input.GetKey(KeyCode.S)) CurrentSpeed -= Accel * Time.deltaTime;
        else CurrentSpeed -= CurrentSpeed /MaxSpeed * Accel * 2 * Time.deltaTime;
        CurrentSpeed = Mathf.Max(0, Mathf.Min(MaxSpeed, CurrentSpeed));

        transform.position += transform.forward * CurrentSpeed;
	}
}
